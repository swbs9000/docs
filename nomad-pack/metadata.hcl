app {
  url = "https://github.com/docmoa/docs"
  author = "my docmoa"
}

pack {
  name = "vuepress"
  description = "vuepress test."
  url = "https://github.com/hashicorp/nomad-pack-community-registry/nginx"
  version = "0.0.1"
}