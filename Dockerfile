FROM blasteh/vuepress

ADD docs /root/src/docs
#ADD node_modules /root/src/node_modules

WORKDIR /root/src

ENTRYPOINT ["/usr/local/bin/npm", "run", "dev"]
