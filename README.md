# docmoa

## Contributing [![contributions welcome](https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=flat)](https://github.com/docmoa/docs)

docmoa (https://docmoa.github.io) 소스페이지 입니다.

## Change Log

### 2021-09-26
- Bug fix
  - Sidebar
    - 한글, 특수문자, 공백 등의 URI 변환을 위해 sidebar생성시 `encodeURI` 적용
- Package
  - Vuepress algolia 패키지 제거